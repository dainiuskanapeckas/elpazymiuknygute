@extends("layouts.app")

@section('content')
  <h1 class="mt-2">Redaguoti paskaitą "{{ $lecture->name }}"</h1>
        {{-- Klaidu isvedimas pagal laravelio validatoriu--}}
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('lecture.update', $lecture->id) }}" method="POST">
            {{ csrf_field() }}
            <div class="row mb-3">
                <div class="col">
                      <label for="name">Pavadinimas</label>
                     <input type="text" name="name" class="form-control" value="{{ $lecture->name }}" placeholder="Pavadinimas">   {{-- tikrinimas ar uzpildyti laukeliai vyksta per situos name= --}}
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                      <label for="description">Aprašymas</label>
                      <input type="text" name="description" class="form-control" value="{{ $lecture->description }}" placeholder="Aprašymas">
                </div>
            </div>
            <input class="btn btn-success" type="submit">
        </form>
    </div>
@endsection
