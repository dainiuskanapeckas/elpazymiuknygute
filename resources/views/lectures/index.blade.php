@extends("layouts.app")

@section('content')
  @if (session('zinute'))
      <div class="alert alert-success mt-1">
          {{ session('zinute') }}
      </div>
@endif
<p>Viso paskaitų {{ $lecturesCount }}</p>
<table class="table table-striped mt-1">
  <thead>
    <tr>
      <th scope="col">Pavadinimas</th>
      <th scope="col">Aprašymas</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    @foreach ($lectures as $lecture) {{-- is kontrolerio gaunam $lectures kuriame yra visos paskaitos ir einam per jas ciklu --}}
    <tr>
      <td>{{ $lecture->name }}</td>
      <td>{{ $lecture->description }}</td>
      <td>
          <a class="btn btn-info" href="{{ route('lecture.edit', $lecture->id) }}">Redaguoti</a>
      </td>
      <td>
          <form action="{{ route('lecture.destroy', $lecture->id) }}" method="POST">
		          {{ csrf_field() }}
		          <input type="submit" class="btn btn-danger" value="Trinti">
	        </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
<a class="btn btn-info ml-4" href="{{ route('lecture.create') }}">Pridėti</a>
<a class="btn btn-info ml-4" href="{{ route('students.list') }}">Studentų sąrašas</a>
@endsection
