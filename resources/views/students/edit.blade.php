@extends("layouts.app")

@section('content')
  <h1>Redaguoti studenta</h1>
        {{-- Klaidu isvedimas pagal laravelio validatoriu--}}
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('student.update', $student->id) }}" method="POST">
            {{ csrf_field() }}
            <div class="row mb-3">
                <div class="col">
                      <label for="name">Vardas</label>
                     <input type="text" name="name" class="form-control" value="{{ $student->name }}" placeholder="Vardas">   {{-- tikrinimas ar uzpildyti laukeliai vyksta per situos name= --}}
                </div>
                <div class="col">
                      <label for="surname">Pavardė</label>
                      <input type="text" name="surname" class="form-control" value="{{ $student->surname }}" placeholder="Pavarde">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col">
                      <label for="email">El. Paštas</label>
                      <input type="text" name="email" class="form-control" value="{{ $student->email }}" placeholder="El. Paštas">
                </div>
                <div class="col">
                      <label for="phone">Tel. Numeris</label>
                      <input type="text" name="phone" class="form-control" value="{{ $student->phone }}" placeholder="Tel. Numeris">
                </div>
            </div>
            <input class="btn btn-success" type="submit">
        </form>
    </div>
@endsection
