@extends("layouts.app")

@section('content')
<h1 class="mt-3 mb-3">Ivesti naują pažimį</h1>
{{-- Klaidu isvedimas pagal laravelio validatoriu--}}
@if  ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach  ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="input-group mb-3">
    <div class="input-group-prepend">
        <label class="input-group-text" for="pasirinkPaskaita">Pasirinkite paskaitą</label>
    </div>
    <select class="custom-select" id="pasirinkPaskaita" name="lectureid" form="gradeCreate">{{-- is cia paimam lectureid pries issiunciant is formos postu--}}
    @foreach  ($lecturesAll as $lecture)
        <option value="{{ $lecture->id }}">{{ $lecture->name }}</option>
    @endforeach
  </select>
</div>
<div class="input-group mb-3">
    <div class="input-group-prepend">
        <label class="input-group-text" for="pasirinkStudenta">Pasirinkite studentą</label>
    </div>
    <select class="custom-select" id="pasirinkStudenta" name="studentid" form="gradeCreate"> {{-- is cia paimam studentid pries issiunciant is formos postu--}}
    @foreach  ($studentsAll as $student)
        <option value="{{ $student->id }}">{{ $student->name }} {{ $student->surname }}</option>
    @endforeach
  </select>
</div>
<div class="input-group mb-3">
    <div class="input-group-prepend">
        <label class="input-group-text" for="pasirinkBala">Įvertinimas</label>
    </div>
    <select class="custom-select" id="pasirinkBala" name="grade" form="gradeCreate"> {{-- is cia paimam studentid pries issiunciant is formos postu--}}
    @for ($i = 1; $i <= 10; $i++)
        <option value="{{ $i }}">{{ $i }}</option>
    @endfor
  </select>
</div>
<form action="{{ route('grade.store') }}" method="POST" id="gradeCreate">
    {{ csrf_field() }}
    <input type="hidden" name="from" value="all">
    <input type="submit" class="btn btn-danger" value="Įvesti">
    </form>
    @endsection
