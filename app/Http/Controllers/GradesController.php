<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grade;
use App\Lecture;
use App\Student;

class GradesController extends Controller
{
    public function index()
    {
        $grades = Grade::orderBy('id', "ASC")->get();
        $gradesCount = Grade::count();

        return view("grades.index", [
                "grades" => $grades,
                "gradesCount" => $gradesCount,
            ]);
    }
    public function destroy(Request $request)
    {
        $grade = Grade::find($request->id);
        $grade->delete();

        //cia tikrinu ar atejom i metoda is vieno student pazymiu ar is visu pazymiu ir ju pazymiu saraso
        //ir pagal tai siunciam atgal i ta pati sarasa po istrynimo su zinute apie sekmingai atlikta veiksma.
        if ($request->from === 'visuStudentu') {
            return redirect()->route('grades.list')->with(
              'zinute',
              'Pažimys ' . $grade->grade . ', kuris priklausė studentui ' . $grade->student->name . ' ' . $grade->student->surname . ' sėkmingai ištrinta'
          );
        } elseif ($request->from === 'vienoStudento') {
            return redirect()->route('grades.show', $grade->student->id)->with( //cia reikia paduoti studento kurio sarasa rodom id
              'zinute',
              'Pažimys ' . $grade->grade . ' už paskaitą ' . $grade->lecture->name . ' sėkmingai ištrintas'
          );
        }
    }
    public function create()
    {
        $studentsAll = Student::all();
        $lecturesAll = Lecture::all();

        return view("grades.create", [ "studentsAll" => $studentsAll, "lecturesAll" => $lecturesAll ]);
    }
    public function createforone($id) //sita funkcija turi iskviesti forma vieno studento pazymio ivedimui
    {
        $student = Student::find($id);
        $lecturesAll = Lecture::all();

        return view("grades.createforone", [ "student" => $student, "lecturesAll" => $lecturesAll ]); //i viewa perduodam vieno studento objekta pagal id
    }
    public function store(Request $request) //cia ateina requestas be jokiu metodu pasinimo, automatiskai paima viska kas yra su requestu kartu!!!
    {
        $atejomIs = $request->from; //one jeigu is vieto studneto, all jeigu is visu saraso
        $messages = [
              'required' => 'O ka čia darai?'
          ];

        $validatedData = $request->validate([ //cia dar reiketu patikrinti pagal regexa, kad neateitu duomenys neteisingi, bet gal laravelis apsaugo nuo sito su csrf_field ?
              'grade' => 'required|numeric',
              'lectureid' => 'required|numeric',
              'studentid' => 'required|numeric'
          ], $messages);

        $grade = new Grade();
        $grade->grade = $request->grade;
        $grade->lecture_id = $request->lectureid;
        $grade->student_id = $request->studentid;
        $grade->save();

        //cia reikia patikrinti is kur ateinam ir siusti i atitinkama sarasa visu arba vieno studento.
        //dabar pridejus pazymi vienam studentui - gryztama i visu sarasa.
        if ($atejomIs === "all") {
            return redirect()->route('grades.list')->with('zinute', $grade->student->name . ' '
              . $grade->student->surname . ' sėkmingai įvertintas ');
        } else if ($atejomIs === "one") {
            return redirect()->route('grades.show', $request->studentid )->with('zinute', $grade->student->name . ' '
            . $grade->student->surname . ' sėkmingai įvertintas ' . $request->grade . ' už ' . $grade->lecture->name );
        }
    }
    public function show($id)
    {
        $student = Student::find($id);

        return view("grades.show", [ "student" => $student ]);
    }
}
