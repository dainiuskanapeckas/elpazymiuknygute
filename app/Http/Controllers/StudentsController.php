<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Gauname is newsItem Modelio visas naujienas ir issaugome i kintamaji
        $students = Student::orderBy('surname', "ASC")->get();
        // Gaunu naujienu skaiciu
        $studentsCount = Student::count(); // grazins skaiciu
        //Perduodu studentu masyva ir studentu skaiciu i view`a students.index
        return view("students.index", [
                  "students" => $students,
                  "studentsCount" => $studentsCount,
              ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view( 'students.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $messages = [
              'required' => 'Laukelis :attribute turi buti uzpildytas',
              'name.required' => 'Įveskite vardą!',
              'surname.required' => 'Įveskite pavardę!',
              'email.required' => 'Iveskite elektroninį paštą!',
          ];

      $validatedData = $request->validate([
              'name' => 'required|max:64|min:2',
              'surname' => 'required|max:64|min:2',
              'email' => 'required|email|max:64|min:5',
              'phone' => 'max:32',
          ], $messages);

      $student = new Student();
      $student->name = $request->name;
      $student->surname = $request->surname;
      $student->email = $request->email;
      $student->phone = $request->phone;
      $student->save();

      return redirect()->route('students.list')->with('zinute', 'Sėkmingai pridėtas ' . $request->name . ' ' . $request->surname );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = Student::find($id);

        return view('students.edit', [ 'student' => $student ]); //reiketu naudoti vienaskaita
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Patikriname uzklausos duomenis
        $messages = [
                  'required' => 'Laukelis :attribute turi buti uzpildytas'
              ];

        $validatedData = $request->validate([
                  'name' => 'required|max:64|min:2',
                  'surname' => 'required|max:64|min:2',
                  'email' => 'required|email|max:64|min:5',
                  'phone' => 'required|max:32',
              ], $messages);

        $student = Student::find($id);
        $student->name = $request->name;
        $student->surname = $request->surname;
        $student->email = $request->email;
        $student->phone = $request->phone;

        $student->save();

        return redirect()->route('students.list')->with('zinute', 'Sėkmingai atnaujintas ' . $request->name . ' ' . $request->surname );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::find($id);
        $student->delete();
        //	Session::flash( 'status', 'Naujiena sekminga istrinta' );
        return redirect()->route('students.list')->with('zinute', 'Sėkmingai ištrintas ' . $student->name . ' ' . $student->surname );;
    }
}
